/*
 * Clase Operaciones
 * Operaciones matemáticas de la calculadora simple
 * Escrito por: Juan Carlos Achig 
 * Email: jac_903@hotmail.com
 * Tarea Ingeniería de Software
 * UTPL 2019
 */
package calculadoraSimple;

public class Operaciones {
	private double a;
	private double b;
	public Operaciones(double a, double b) {
		this.a = a;
		this.b = b;
	}
	public double suma() {
		return this.a + this.b;
	}
	public double resta() {
		return this.a - this.b;
	}
	public double multiplicacion() {
		return this.a * this.b;
	}
	public double division() {
		return this.a / this.b;
	}
}
