/*
 * Clase Interfaz
 * Vista de la calculadora Simple
 * Escrito por: Juan Carlos Achig 
 * Email: jac_903@hotmail.com
 * Tarea Ingeniería de Software
 * UTPL 2019
 */
package calculadoraSimple;

import java.util.Scanner;

public class Interfaz {
	private double x;
	private double y;
	private char s;
	public void menu() {
		Scanner inputDouble = new Scanner (System.in);
		Scanner inputString = new Scanner (System.in);
		System.out.println("******************");
		System.out.println("Calculadora simple");
		System.out.println("     v.0.0.2");
		System.out.println("******************");
		System.out.println();
		System.out.println("Hola usuario, escriba dos valores; \nusar coma(,) para indicar valores decimales.\nLa operación a realizar:\n+ : Suma\n* : Multiplicación");
		System.out.println();
		try {
			System.out.print("Valor x: ");
			this.setX(inputDouble.nextDouble());
			System.out.print("Valor y: ");
			this.setY(inputDouble.nextDouble());
			System.out.print("Operación a realizar: ");
			this.setS(inputString.next().charAt(0));
			Operaciones op = new Operaciones(this.x,this.y);
			inputDouble.close();
			inputString.close();
			Archivos a = Archivos.getInstancia();
			switch (s) {
			case '+' :
				System.out.print("SUMA: ");
				System.out.println(op.suma());
				a.modificarArchivo(this.formatoResultado('+', op.suma()));
				break;
			case '-' :
				System.out.print("RESTA: ");
				System.out.println(op.resta());
				a.modificarArchivo(this.formatoResultado('-', op.resta()));
				break;
			case '*' :
				System.out.print("MULTIPLICACIÓN: ");
				System.out.println(op.multiplicacion());
				a.modificarArchivo(this.formatoResultado('*', op.multiplicacion()));
				break;
			case '/' :
				System.out.print("DIVISIÓN: ");
				System.out.println(op.division());
				a.modificarArchivo(this.formatoResultado('*', op.division()));
				break;
			default :
				System.out.println("Ingresó una operación inválida\nBye!!! ");
			}
		}catch(Exception e) {
			System.err.println(e);
			System.exit(0);
		}
	}
	private String formatoResultado(char s, double resul) {
		return String.valueOf(this.x) + ' ' + s + ' ' + String.valueOf(this.y) + " = " + String.valueOf(resul) + "\n";
	}
	private void setX(double x) {
		this.x = x;
	}
	private void setY(double y) {
		this.y = y;
	}
	private void setS(char s) {
		this.s = s;
	}
}
