/*
 * Clase Principal
 * Escrito por: Juan Carlos Achig 
 * Email: jac_903@hotmail.com
 * Tarea Ingeniería de Software
 * UTPL 2019
 */
package calculadoraSimple;

import java.io.IOException;

public class Principal {
	public static void main(String[] args) throws IOException {
		Archivos a = Archivos.getInstancia();
		boolean exito = false;
		if (a.verificarAchivo()) {
			exito = true;
		}else {
			if(a.crearArchivo()) {
				exito = true;
			}
		}
		if (exito) {
			Interfaz Pantalla = new Interfaz();
			Pantalla.menu();
		}
	}
}