package calculadoraSimple;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Archivos {
	private static Archivos instancia;
	private File directorio;
	private File archivo;
	private String nombreDirectorio;
	private String nombreArchivo;
	private Archivos() throws IOException {
		this.directorio = new File (".");
		this.nombreDirectorio = this.directorio.getCanonicalPath();
		this.nombreArchivo = this.nombreDirectorio + "/RegistroTareaIngSW.txt";
		this.archivo = new File (this.nombreArchivo);
	}
	public static Archivos getInstancia() throws IOException {
		if (instancia == null)
			instancia = new Archivos();
		return instancia;
	}
	public boolean verificarAchivo() {
		try {
			if (this.archivo.exists()) {
				return true;
			}
			else {
				return false;
			}
		}
		catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}
	public boolean crearArchivo() throws IOException {
		if (this.archivo.createNewFile()) {
			return true;
		}else {
			return false;
		}
	}
	public boolean modificarArchivo(String c) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(this.nombreArchivo, true));
		bw.write(c);
		bw.close();		
		return false;
	}

}
